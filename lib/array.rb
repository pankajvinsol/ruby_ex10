class Array
  
  def group_by_length
    group_by { |value| value.to_s.length }
  end

  def group_by_even_odd_length
    even_odd_group = group_by_length.values.group_by { |value| value[0].to_s.length.even? ? :even : :odd }
    even_odd_group.map { |key, values| values.sort_by! { |value| value[0].to_s.length } }
    even_odd_group
  end

end

